use std::{
    fs::OpenOptions,
    io::{self, Write},
    ops::Not,
};

use crate::many::{AlignItems, FlexDirection, GridGap, JustifyContent, ManyType};

use super::*;

pub fn gen_css(root: &'static str, css: style::StyleSheet) -> io::Result<()> {
    let mut f = OpenOptions::new()
        .create(true)
        .truncate(true)
        .write(true)
        .open([root, "/style.css"].concat())?;

    for (sel, rule) in css.0 {
        writeln!(f, "{sel} {{\n{}\n}}", rule.join("\n"))?;
    }

    Ok(())
}

pub fn gen_view(
    root: &'static str,
    name: &'static str,
    View {
        script,
        elm,
        use_common,
    }: View,
    common: &Element,
) -> io::Result<()> {
    let mut f = OpenOptions::new()
        .create(true)
        .truncate(true)
        .write(true)
        .open([root, "/", name, ".html"].concat())?;

    writeln!(
        f,
        "<!DOCTYPE html>\n\
        <html lang=\"en\">\n\
        <head>\n\
            <meta charset=\"UTF-8\">\n\
            <title></title>\n\
            <link rel=\"stylesheet\" href=\"style.css\" media=\"all\">\n\
            <script>\n{script}\n</script>\n\
        </head>\n\
        <body>"
    )?;

    if use_common {
        gen_common(&mut f, common, &elm)?;
    } else {
        gen_elm(&mut f, &elm)?;
    }

    writeln!(f, "</body>\n</html>")
}

fn gen_common(f: &mut impl Write, common: &Element, elm: &Element) -> io::Result<()> {
    match common.content {
        Content::Many(_, ref v) => {
            let attr = gen_attr(common);
            writeln!(f, "<div {attr}>")?;
            for e in v {
                gen_common(f, e, elm)?;
            }
            writeln!(f, "</div>")?;
            Ok(())
        }
        Content::View => gen_elm(f, elm),
        _ => gen_elm(f, common),
    }
}

fn gen_elm(f: &mut impl Write, elm: &Element) -> io::Result<()> {
    let attr = gen_attr(elm);
    match &elm.content {
        Content::Text(t) => writeln!(f, "<span {attr}>{t}</span>"),
        Content::Header(l, c) => writeln!(f, "<{l} {attr}>{c}</{l}>"),
        Content::Paragraph(c) => writeln!(f, "<p {attr}>{c}</p>"),
        Content::Link(href, c) => writeln!(f, "<a {attr} href=\"{href}\">{c}</a>"),
        Content::Button(on_click, t) => {
            writeln!(f, "<button {attr} onclick=\"{on_click}\">{t}</button>")
        }
        Content::Form(form) => gen_form(f, elm, form),
        Content::Nav(v) => {
            writeln!(f, "<nav {attr}>")?;
            let mut scripts = Vec::new();
            for (i, (l, c, script)) in v.iter().enumerate() {
                if let Some(script) = script {
                    writeln!(f, "<a href=\"{l}.html\" hidden>{c}</a>")?;
                    scripts.push((i, script));
                } else {
                    writeln!(f, "<a href=\"{l}.html\">{c}</a>")?;
                }
            }
            if scripts.is_empty().not() {
                if elm.id.is_empty() {
                    panic!("nav with optional routes and no id");
                }
                writeln!(
                    f,
                    "<script>\nlet links = document.getElementById(\"{}\").children;",
                    elm.id
                )?;
                for (i, s) in scripts {
                    writeln!(f, "if ({s}) links.item({i}).hidden = false;",)?;
                }
                writeln!(f, "</script>")?;
            }
            writeln!(f, "</nav>")
        }
        Content::Many(_, v) => {
            writeln!(f, "<div {attr}>")?;
            for e in v {
                gen_elm(f, e)?;
            }
            writeln!(f, "</div>")?;
            Ok(())
        }
        Content::View => unimplemented!(),
        Content::None => Ok(()),
    }
}

fn gen_form(
    f: &mut impl Write,
    elm: &Element,
    elms::Form {
        many,
        on_submit,
        inputs,
        submit_label,
    }: &elms::Form,
) -> io::Result<()> {
    let mut attr = gen_attr(elm);
    if attr.contains("style") {
        attr.pop();
        gen_many(&mut attr, many);
    } else {
        attr.push_str("style=\"");
        gen_many(&mut attr, many);
    }
    attr.push('"');

    writeln!(
        f,
        "<form {attr}\nonsubmit=\"\n\
        event.preventDefault();\n\
        let v = new FormData(event.target);\n\
        {on_submit}\n\
        \">"
    )?;

    for (
        i,
        elms::FormInput {
            name,
            label,
            typ,
            class,
            required,
        },
    ) in inputs.iter().enumerate()
    {
        let typ = match typ {
            elms::InputType::Text => "text",
            elms::InputType::Password => "password",
            elms::InputType::Date => "date",
            elms::InputType::Time => "time",
            elms::InputType::DateTime => "datetime",
            elms::InputType::Button => "button",
            elms::InputType::Custom(s) => s,
        };

        writeln!(
            f,
            "<label for=\"{name}-{i}\">{label}</label>\n\
            <input id=\"{name}-{i}\"{} name=\"{name}\" type=\"{typ}\" {}/>",
            class
                .is_empty()
                .not()
                .then(|| ["style=\"", class, "\""].concat())
                .unwrap_or_default(),
            required.then_some("required").unwrap_or_default()
        )?;
    }

    write!(f, "<button")?;
    if let ManyType::Grid(many::Grid { columns, .. }) = many {
        write!(f, " style=\"grid-column: span {columns}\" ")?;
    }
    writeln!(f, "type=\"submit\">{submit_label}</button>")?;

    writeln!(f, "</form>")
}

fn gen_attr(elm: &Element) -> String {
    let mut attr = String::new();
    gen_id(&mut attr, elm.id);
    gen_class(&mut attr, elm.class);

    if elm.style.is_empty()
        && matches!(elm.pad, Padding::None)
        && match elm.content {
            Content::Many(ManyType::Basic, _) => true,
            Content::Many(_, _) => false,
            _ => true,
        } {
        return attr;
    }

    attr.push_str("style=\"");
    attr.push_str(&elm.style);
    attr.push(' ');
    gen_pad(&mut attr, &elm.pad);
    if let Content::Many(ref t, _) = elm.content {
        gen_many(&mut attr, t);
    }
    attr.push('"');
    attr
}

fn gen_id(s: &mut String, id: &'static str) {
    if id.is_empty().not() {
        s.push_str("id=\"");
        s.push_str(id);
        s.push('"');
    }
}

fn gen_class(s: &mut String, class: &'static str) {
    if class.is_empty().not() {
        s.push_str("class=\"");
        s.push_str(class);
        s.push('"');
    }
}

fn gen_many(s: &mut String, mt: &ManyType) {
    match mt {
        ManyType::Basic => (),
        ManyType::Flex(many::Flex {
            dir,
            justify_content,
            align_items,
        }) => {
            s.push_str("display: flex; ");
            if dir == &FlexDirection::Column {
                s.push_str("flex-direction: column; ");
            }
            gen_many_common(s, justify_content, align_items);
        }
        ManyType::Grid(many::Grid {
            columns,
            gap,
            justify_content,
            align_items,
        }) => {
            s.push_str("display: grid; ");

            match gap {
                GridGap::All(a) => {
                    s.push_str("gap: ");
                    s.push_str(to_rem(a));
                    s.push(';');
                }
                GridGap::XY(x, y) => {
                    s.push_str("gap: ");
                    s.push_str(to_rem(x));
                    s.push(' ');
                    s.push_str(to_rem(y));
                    s.push(';');
                }
                GridGap::None => (),
            }

            s.push_str("grid-template-columns: repeat(");
            s.push_str(&columns.to_string());
            s.push_str(", auto);");

            gen_many_common(s, justify_content, align_items);
        }
    }
}

fn gen_many_common(s: &mut String, justify_content: &JustifyContent, align_items: &AlignItems) {
    s.push_str("justify-content: ");
    match justify_content {
        JustifyContent::Start => s.push_str("start"),
        JustifyContent::End => s.push_str("end"),
        JustifyContent::Center => s.push_str("center"),
        JustifyContent::SpaceBetween => s.push_str("space-between"),
        JustifyContent::SpaceAround => s.push_str("space-around"),
        JustifyContent::Stretch => s.push_str("stretch"),
    }
    s.push(';');

    s.push_str("align-items: ");
    match align_items {
        AlignItems::Start => s.push_str("start"),
        AlignItems::End => s.push_str("end"),
        AlignItems::Center => s.push_str("center"),
    }
    s.push(';');
}

fn gen_pad(s: &mut String, p: &Padding) {
    let str = match p {
        Padding::All(n) => [":", to_rem(n)].concat(),
        Padding::XY(x, y) => [":", to_rem(x), " ", to_rem(y)].concat(),
        Padding::TRBL(t, r, b, l) => [":", to_rem(t), to_rem(r), to_rem(b), to_rem(l)].concat(),
        Padding::Top(v) => ["-top:", to_rem(v)].concat(),
        Padding::Right(v) => ["-right:", to_rem(v)].concat(),
        Padding::Bottom(v) => ["-bottom:", to_rem(v)].concat(),
        Padding::Left(v) => ["-left:", to_rem(v)].concat(),
        Padding::None => return,
    };
    s.push_str("padding");
    s.push_str(&str);
    s.push(';');
}
