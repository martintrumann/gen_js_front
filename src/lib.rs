pub use style::Padding;

pub mod elms;
pub mod many;
pub mod style;

pub mod fetch;

mod gen;
pub fn gen(root: &'static str, app: App) -> Result<(), std::io::Error> {
    gen::gen_css(root, app.style)?;
    for (n, v) in app.views {
        gen::gen_view(root, n, v, &app.common)?;
    }
    Ok(())
}

pub struct App {
    pub style: style::StyleSheet,
    pub common: Element,
    pub views: Vec<(&'static str, View)>,
}

pub struct View {
    pub script: String,
    pub elm: Element,
    pub use_common: bool,
}

#[derive(Debug, Default)]
pub struct Element {
    pub pad: Padding,
    pub id: &'static str,
    pub class: &'static str,
    pub style: String,
    pub cond: Option<String>,
    pub content: Content,
}

impl Element {
    pub fn style(mut self, s: impl style::Style) -> Self {
        let s = s.to_css();
        self.style.push_str(&s);
        if !s.ends_with(';') {
            self.style.push(';')
        }
        self
    }

    pub fn class(mut self, class: &'static str) -> Self {
        self.class = class;
        self
    }

    pub fn pad(mut self, pad: Padding) -> Self {
        self.pad = pad;
        self
    }

    pub fn id(mut self, id: &'static str) -> Self {
        self.id = id;
        self
    }
}

#[derive(Debug, Default)]
pub enum Content {
    Text(&'static str),
    Header(HeaderLevel, &'static str),
    Paragraph(&'static str),
    Link(&'static str, &'static str),
    Button(String, &'static str),
    Form(elms::Form),
    Nav(Vec<(&'static str, &'static str, Option<String>)>),
    Many(many::ManyType, Vec<Element>),
    View,
    #[default]
    None,
}

#[derive(Debug)]
pub enum HeaderLevel {
    H1,
    H2,
    H3,
    H4,
    H5,
    H6,
}

impl std::fmt::Display for HeaderLevel {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            HeaderLevel::H1 => write!(f, "h1"),
            HeaderLevel::H2 => write!(f, "h2"),
            HeaderLevel::H3 => write!(f, "h3"),
            HeaderLevel::H4 => write!(f, "h4"),
            HeaderLevel::H5 => write!(f, "h5"),
            HeaderLevel::H6 => write!(f, "h6"),
        }
    }
}

pub fn to_rem(num: &u8) -> &'static str {
    match num {
        0 => "0",
        1 => "0.25rem",
        2 => "0.5rem",
        3 => "0.75rem",
        4 => "1rem",
        5 => "1.5rem",
        6 => "2rem",
        7 => "3rem",
        8 => "4rem",
        _ => todo!(),
    }
}
