use std::ops::{Add, AddAssign};

use crate::to_rem;

pub trait Style {
    fn to_css(self) -> String;
}

#[derive(Debug, Default)]
pub struct StyleSheet(pub(crate) Vec<(&'static str, Vec<String>)>);

pub struct Rules(&'static str, Vec<String>);

pub fn select(s: &'static str) -> Rules {
    Rules::new(s)
}

impl StyleSheet {
    pub fn push(mut self, r: impl Into<Rules>) -> Self {
        let r = r.into();
        self.0.push((r.0, r.1));
        self
    }
}

impl<T: IntoIterator<Item = Rules>> From<T> for StyleSheet {
    fn from(v: T) -> Self {
        Self(v.into_iter().map(|r| (r.0, r.1)).collect())
    }
}

impl<T: IntoIterator<Item = Rules>> Add<T> for StyleSheet {
    type Output = Self;
    fn add(mut self, rhs: T) -> Self {
        self.0.extend(rhs.into_iter().map(|r| (r.0, r.1)));
        self
    }
}

impl Add<Rules> for StyleSheet {
    type Output = Self;
    fn add(mut self, rhs: Rules) -> Self {
        self.0.push((rhs.0, rhs.1));
        self
    }
}

impl<T: IntoIterator<Item = Rules>> AddAssign<T> for StyleSheet {
    fn add_assign(&mut self, rhs: T) {
        self.0.extend(rhs.into_iter().map(|r| (r.0, r.1)));
    }
}

impl Rules {
    pub fn new(selector: &'static str) -> Self {
        Self(selector, Vec::new())
    }

    pub fn push(&mut self, r: impl Style) {
        self.1.push(r.to_css());
    }
}

impl<S: Style> Add<S> for Rules {
    type Output = Self;
    fn add(mut self, r: S) -> Self {
        self.1.push(r.to_css());
        self
    }
}

impl<S: Style> AddAssign<S> for Rules {
    fn add_assign(&mut self, r: S) {
        self.1.push(r.to_css());
    }
}

#[derive(Debug, Default)]
pub enum Padding {
    All(u8),
    XY(u8, u8),
    TRBL(u8, u8, u8, u8),
    Top(u8),
    Right(u8),
    Bottom(u8),
    Left(u8),
    #[default]
    None,
}
impl Style for Padding {
    fn to_css(self) -> String {
        let str = match &self {
            Padding::All(n) => [":", to_rem(n)].concat(),
            Padding::XY(x, y) => [":", to_rem(x), " ", to_rem(y)].concat(),
            Padding::TRBL(t, r, b, l) => {
                [":", to_rem(t), to_rem(r), to_rem(b), to_rem(l)].join(" ")
            }
            Padding::Top(v) => ["-top:", to_rem(v)].concat(),
            Padding::Right(v) => ["-right:", to_rem(v)].concat(),
            Padding::Bottom(v) => ["-bottom:", to_rem(v)].concat(),
            Padding::Left(v) => ["-left:", to_rem(v)].concat(),
            Padding::None => return String::new(),
        };

        format!("padding{str};")
    }
}

impl Style for &'static str {
    fn to_css(self) -> String {
        self.into()
    }
}

pub enum Height {
    Full,
    Px(u16),
    Unit(u8),
}

impl Style for Height {
    fn to_css(self) -> String {
        match self {
            Height::Full => "height: 100vh;".into(),
            Height::Px(p) => format!("height: {p}px;"),
            Height::Unit(u) => format!("height: {};", to_rem(&u)),
        }
    }
}

pub enum TextAlign {
    Start,
    Center,
    End,
}
impl Style for TextAlign {
    fn to_css(self) -> String {
        match self {
            TextAlign::Start => "text-align: start;",
            TextAlign::Center => "text-align: center;",
            TextAlign::End => "text-align: end;",
        }
        .into()
    }
}

pub struct GridSpan(u8);

impl Style for GridSpan {
    fn to_css(self) -> String {
        format!("grid-col: span {};", self.0)
    }
}
