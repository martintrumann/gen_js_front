use std::ops::Not;

pub struct Fetch {
    pub url: String,
    pub method: &'static str,
    pub headers: Vec<(&'static str, &'static str)>,
    pub body: &'static str,
    pub ok: &'static str,
    pub err: &'static str,
    pub catch: &'static str,
}

impl Default for Fetch {
    fn default() -> Self {
        Self {
            url: "".to_string(),
            method: "GET",
            headers: Vec::new(),
            body: "",
            ok: "",
            err: "",
            catch: "",
        }
    }
}

impl Fetch {
    pub fn to_js(&self) -> String {
        let mut out = format!("fetch('{}', {{\nmethod: '{}',\n", self.url, self.method);

        if self.headers.is_empty().not() {
            out.push_str("headers: {\n");
            for (k, v) in self.headers.iter() {
                out.push_str(k);
                out.push_str(": '");
                out.push_str(v);
                out.push_str("',");
            }
            out.push_str("\n},\n");
        }

        if self.body.is_empty().not() {
            out.push_str("body: JSON.stringify(");
            out.push_str(self.body);
            out.push_str("),\n")
        }

        out.push_str("}).then(res => {\nif (res.ok) {\nres.json().then(");
        out.push_str(self.ok);
        out.push_str(")\n} else {\nres.text().then(");
        out.push_str(self.err);
        out.push_str(")\n}\n})");

        if self.catch.is_empty().not() {
            out.push_str(".catch(");
            out.push_str(self.catch);
            out.push(')');
        }
        out
    }
}

#[test]
fn test_fetch() {
    let mut f = Fetch {
        url: "test".to_string(),
        method: "GET",
        ok: "data => {console.log(data)}",
        err: "err => {console.error(err)}",
        ..Default::default()
    };

    assert_eq!(
        f.to_js(),
        "fetch('test', {
            method: 'GET',
        }).then(res => {
            if (res.ok) {
                res.json().then(data => {console.log(data)})
            } else {
                res.text().then(err => {console.error(err)})
            }
        })"
        .replace('\t', "")
    );

    f.headers.push(("Auth", "test"));
    assert_eq!(
        f.to_js(),
        "fetch('test', {
            method: 'GET',
            headers: {
                Auth: 'test',
            },
        }).then(res => {
            if (res.ok) {
                res.json().then(data => {console.log(data)})
            } else {
                res.text().then(err => {console.error(err)})
            }
        })"
        .replace('\t', "")
    );

    f.body = "'test'";
    assert_eq!(
        f.to_js(),
        "fetch('test', {
            method: 'GET',
            headers: {
                Auth: 'test',
            },
            body: JSON.stringify('test'),
        }).then(res => {
            if (res.ok) {
                res.json().then(data => {console.log(data)})
            } else {
                res.text().then(err => {console.error(err)})
            }
        })"
        .replace('\t', "")
    );
}
