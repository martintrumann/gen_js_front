#[derive(Debug, Default)]
pub enum ManyType {
    #[default]
    Basic,
    Flex(Flex),
    Grid(Grid),
}

macro_rules! impl_mt {
    ($ident:ident) => {
        impl From<$ident> for ManyType {
            fn from(v: $ident) -> Self {
                Self::$ident(v)
            }
        }
    };
}

impl_mt!(Flex);
impl_mt!(Grid);

#[derive(Debug, PartialEq, Eq)]
pub struct Flex {
    pub dir: FlexDirection,
    pub justify_content: JustifyContent,
    pub align_items: AlignItems,
}

#[derive(Debug, PartialEq, Eq)]
pub enum FlexDirection {
    Row,
    Column,
}

#[derive(Debug, PartialEq, Eq)]
pub enum JustifyContent {
    Start,
    End,
    Center,
    SpaceBetween,
    SpaceAround,
    Stretch,
}

#[derive(Debug, PartialEq, Eq)]
pub enum AlignItems {
    Start,
    End,
    Center,
}

#[derive(Debug)]
pub struct Grid {
    pub columns: u8,
    pub gap: GridGap,
    pub justify_content: JustifyContent,
    pub align_items: AlignItems,
}

impl Default for Grid {
    fn default() -> Self {
        Self {
            columns: 1,
            gap: GridGap::None,
            justify_content: JustifyContent::Stretch,
            align_items: AlignItems::Start,
        }
    }
}

#[derive(Debug, Default)]
pub enum GridGap {
    All(u8),
    XY(u8, u8),
    #[default]
    None,
}
