use crate::{
    many::{AlignItems, Flex, FlexDirection, Grid, JustifyContent, ManyType},
    Element,
};

pub trait IntoElm {
    fn into_elm(self) -> Element;
}

impl<T: IntoElm> From<T> for Element {
    fn from(value: T) -> Self {
        value.into_elm()
    }
}

pub struct Div(ManyType, Vec<Element>);

impl Div {
    pub const CENTER: Self = Div(
        ManyType::Flex(Flex {
            dir: FlexDirection::Column,
            justify_content: JustifyContent::Center,
            align_items: AlignItems::Center,
        }),
        Vec::new(),
    );

    pub fn new() -> Self {
        Self(ManyType::Basic, Vec::new())
    }

    pub fn grid(g: Grid) -> Self {
        Self(ManyType::Grid(g), Vec::new())
    }

    pub fn flex(f: Flex) -> Self {
        Self(ManyType::Flex(f), Vec::new())
    }

    pub fn elm(mut self, e: impl Into<Element>) -> Self {
        self.1.push(e.into());
        self
    }
}

impl Default for Div {
    fn default() -> Self {
        Self::new()
    }
}

impl IntoElm for Div {
    fn into_elm(self) -> Element {
        Element {
            content: crate::Content::Many(self.0, self.1),
            ..Default::default()
        }
    }
}

#[derive(Debug)]
pub struct Form {
    pub many: ManyType,
    /// Inserted into `onsubmit=` attribute.
    ///
    /// Should be small and cannot contain `"`.
    pub on_submit: String,
    pub inputs: Vec<FormInput>,
    pub submit_label: &'static str,
}

#[derive(Debug)]
pub struct FormInput {
    pub name: &'static str,
    pub label: &'static str,
    pub class: &'static str,
    pub typ: InputType,
    pub required: bool,
}

impl FormInput {
    #[inline]
    pub fn password(name: &'static str, label: &'static str) -> Self {
        Self::new(name, label, InputType::Password)
    }

    #[inline]
    pub fn text(name: &'static str, label: &'static str) -> Self {
        Self::new(name, label, InputType::Text)
    }

    #[inline]
    pub fn new(name: &'static str, label: &'static str, typ: InputType) -> Self {
        Self {
            name,
            label,
            class: "",
            typ,
            required: false,
        }
    }

    #[inline]
    pub fn class(mut self, class: &'static str) -> Self {
        self.class = class;
        self
    }

    #[inline]
    pub fn required(mut self) -> Self {
        self.required = true;
        self
    }
}

#[derive(Debug)]
pub enum InputType {
    Text,
    Password,

    Date,
    Time,
    DateTime,

    Button,

    /// TODO: Under construction!
    Custom(String),
}

impl IntoElm for Form {
    fn into_elm(self) -> Element {
        Element {
            content: crate::Content::Form(self),
            ..Default::default()
        }
    }
}

pub struct Nav(Vec<(&'static str, &'static str, Option<String>)>);

impl Nav {
    pub fn new() -> Self {
        Self(Vec::new())
    }

    pub fn link(mut self, loc: &'static str, name: &'static str) -> Self {
        self.0.push((loc, name, None));
        self
    }

    pub fn link_if(mut self, loc: &'static str, name: &'static str, cond: String) -> Self {
        self.0.push((loc, name, Some(cond)));
        self
    }
}

impl Default for Nav {
    fn default() -> Self {
        Self::new()
    }
}

impl IntoElm for Nav {
    fn into_elm(self) -> Element {
        Element {
            content: crate::Content::Nav(self.0),
            ..Default::default()
        }
    }
}

pub struct Header(pub crate::HeaderLevel, pub &'static str);

macro_rules! impl_h {
    ($num:ident, $lev:ident) => {
        pub fn $num(c: &'static str) -> Self {
            Self(crate::HeaderLevel::$lev, c)
        }
    };
}

impl Header {
    impl_h!(h1, H1);
    impl_h!(h2, H2);
    impl_h!(h3, H3);
    impl_h!(h4, H4);
    impl_h!(h5, H5);
    impl_h!(h6, H6);
}

impl IntoElm for Header {
    fn into_elm(self) -> Element {
        Element {
            content: crate::Content::Header(self.0, self.1),
            ..Default::default()
        }
    }
}

pub struct Views;
impl IntoElm for Views {
    fn into_elm(self) -> Element {
        Element {
            content: crate::Content::View,
            ..Default::default()
        }
    }
}

impl IntoElm for &'static str {
    fn into_elm(self) -> Element {
        Element {
            content: crate::Content::Text(self),
            ..Default::default()
        }
    }
}
